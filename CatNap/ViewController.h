//
//  ViewController.h
//  CatNap
//

//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface ViewController : UIViewController

@end
