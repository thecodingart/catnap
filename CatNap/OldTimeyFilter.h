//
//  OldTimeyFilter.h
//  CatNap
//
//  Created by Brandon Levasseur on 1/31/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import <CoreImage/CoreImage.h>

@interface OldTimeyFilter : CIFilter

@property (strong, nonatomic) CIImage *inputImage;

@end
