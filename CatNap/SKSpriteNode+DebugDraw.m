//
//  SKSpriteNode+DebugDraw.m
//  CatNap
//
//  Created by Brandon Levasseur on 12/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "SKSpriteNode+DebugDraw.h"

//disable debug drawing by setting this to NO
static BOOL kDebugDraw = YES;

@implementation SKSpriteNode (DebugDraw)

-(void)attachDebugFrameFromPath:(CGPathRef)bodyPath
{
    if (kDebugDraw==NO) {
        return;
    }
    
    SKShapeNode *shape = [SKShapeNode node];
    
    shape.path = bodyPath;
    shape.strokeColor = [SKColor colorWithRed:1.0 green:0 blue:0 alpha:0.5];
    shape.lineWidth = 1.0;
    
    [self addChild:shape];
}

-(void)attachDebugRectWithSize:(CGSize)s
{
    CGPathRef bodyPath = CGPathCreateWithRect(CGRectMake(-s.width/2, -s.height/2, s.width, s.height), nil);
    [self attachDebugFrameFromPath:bodyPath];
    CGPathRelease(bodyPath);
}

@end
