//
//  MyScene.h
//  CatNap
//

//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol ImageCaptureDelegate

-(void)requestImagePicker;

@end

@interface MyScene : SKScene

@property (nonatomic, assign) id <ImageCaptureDelegate> delegate;

- (instancetype)initWithSize:(CGSize)size andLevelNumber:(int)currentLevel;

-(void)setPhotoTexture:(SKTexture *)texture;

@end
