//
//  ViewController.m
//  CatNap
//
//  Created by Brandon Levasseur on 12/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@interface ViewController () <ImageCaptureDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation ViewController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    NSArray *ciFilters = [CIFilter filterNamesInCategory:kCICategoryBuiltIn];
    for (NSString *filter in ciFilters) {
        NSLog(@"filter %@", [[CIFilter filterWithName:filter] attributes]);
    }
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    
    if (!skView.scene) {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        MyScene * scene = [[MyScene alloc] initWithSize:skView.bounds.size andLevelNumber:1];
        scene.delegate = self;
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - ImageCatureDelegate methods

-(void)requestImagePicker
{
    UIImagePickerController *imagePicker = [UIImagePickerController new];
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        SKTexture *imageTexture = [SKTexture textureWithImage:image];
        
        CIFilter *sepia = [CIFilter filterWithName:@"CISepiaTone"];
        [sepia setValue:@(0.8) forKey:kCIInputIntensityKey];
        imageTexture = [imageTexture textureByApplyingCIFilter:sepia];
        
        SKView *view = (SKView *)self.view;
        MyScene *currentScene = (MyScene *)view.scene;
        [currentScene setPhotoTexture:imageTexture];
    }];
}

@end
