//
//  CustomTransitionFilter.h
//  CatNap
//
//  Created by Brandon Levasseur on 2/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import <CoreImage/CoreImage.h>

@interface CustomTransitionFilter : CIFilter

@property (strong, nonatomic) CIImage *inputImage;
@property (strong, nonatomic) CIImage *inputTargetImage;
@property (assign, nonatomic) NSTimeInterval inputTime;

@end
