//
//  CustomTransitionFilter.m
//  CatNap
//
//  Created by Brandon Levasseur on 2/1/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import "CustomTransitionFilter.h"

@implementation CustomTransitionFilter

- (CIImage *)outputImage
{
    CIFilter *color = [CIFilter filterWithName:@"CIConstantColorGenerator"];
    [color setValue:[CIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:self.inputTime] forKey:kCIInputColorKey];
    
    CIFilter *blendWithMask = [CIFilter filterWithName:@"CIBlendWithAlphaMask"];
    [blendWithMask setValue:color.outputImage forKey:kCIInputMaskImageKey];
    [blendWithMask setValue:self.inputImage forKey:kCIInputBackgroundImageKey];
    [blendWithMask setValue:self.inputTargetImage forKey:kCIInputImageKey];
    
    CIFilter *spinFilter = [CIFilter filterWithName:@"CIAffineTransform"];
    [spinFilter setValue:blendWithMask.outputImage forKey:kCIInputImageKey];
    CGAffineTransform t = CGAffineTransformMakeRotation(self.inputTime * 3.14 * 4.0);
    NSValue *transformationValue = [NSValue valueWithCGAffineTransform:t];
    [spinFilter setValue:transformationValue forKey:kCIInputTransformKey];
    
    return spinFilter.outputImage;
}

@end
