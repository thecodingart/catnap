//
//  OldTVNode.h
//  CatNap
//
//  Created by Brandon Levasseur on 12/31/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface OldTVNode : SKSpriteNode

-(instancetype)initWithRect:(CGRect)frame;

@end
