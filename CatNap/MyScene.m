//
//  MyScene.m
//  CatNap
//
//  Created by Brandon Levasseur on 12/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "MyScene.h"
#import "SKSpriteNode+DebugDraw.h"
#import "SKTAudio.h"
#import "SKTUtils.h"
#import "OldTVNode.h"
#import "Physics.h"
#import "OldTimeyFilter.h"
#import "CustomTransitionFilter.h"

CGPoint adjustedPoint(CGPoint inputPoint, CGSize inputSize)
{
    float width = inputSize.width * .15;
    float height = inputSize.height * .15;
    float xMove = width * RandomFloat() - width / 2.0;
    float yMove = height * RandomFloat() - height / 2.0;
    
    return CGPointMake(inputPoint.x + xMove, inputPoint.y + yMove);
}

@interface MyScene ()<SKPhysicsContactDelegate>

@end

@implementation MyScene
{
    SKNode *_gameNode;
    SKSpriteNode *_catNode;
    SKSpriteNode *_bedNode;
    
    int _currentLevel;
    
    BOOL _isHooked;
    
    SKSpriteNode *_hookBaseNode;
    SKSpriteNode *_hookNode;
    SKSpriteNode *_ropeNode;
    
    SKEffectNode *_blockNode;
    CIFilter *_bumpFilter;
}

- (instancetype)initWithSize:(CGSize)size andLevelNumber:(int)currentLevel
{
    if (self = [super initWithSize:size]) {
        [self initializeSceneWithLevelNumber:currentLevel];
    }
    return self;
}

- (void)didSimulatePhysics
{
    if (_catNode.physicsBody.contactTestBitMask && fabs(_catNode.zRotation) > DegreesToRadians(25)) {
        if(_isHooked == NO)[self lose];
    }
    
    CGFloat angle = CGPointToAngle(CGPointSubtract(_hookBaseNode.position, _hookNode.position));
    
    _ropeNode.zRotation = M_PI + angle;
}

- (void)initializeSceneWithLevelNumber:(int)levelNumber
{
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsWorld.contactDelegate = self;
    self.physicsBody.categoryBitMask = CNPhysicsCategoryEdge;
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    bg.position = CGPointMake(self.size.width/2, self.size.height/2);
    [self addChild:bg];
    [self addCatBed];
    
    SKSpriteNode *bg2 = [SKSpriteNode spriteNodeWithImageNamed:@"background-desat"];
    
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Zapfino"];
    label.text = @"Cat Nap";
    label.fontSize = 96;
    
    SKCropNode *cropNode = [SKCropNode node];
    [cropNode addChild:bg2];
    [cropNode setMaskNode:label];
    cropNode.position = CGPointMake(self.size.width/2, self.size.height/2);
    [self addChild:cropNode];
    
    _gameNode = [SKNode node];
    [self addChild:_gameNode];
    
    _blockNode = [SKEffectNode node];
    _bumpFilter = [CIFilter filterWithName:@"CIBumpDistortion"];
    [_bumpFilter setValue:@1.0 forKey:kCIInputScaleKey];
    [_bumpFilter setValue:[CIVector vectorWithCGPoint:CGPointMake(270, 180)] forKey:kCIInputCenterKey];
    _blockNode.filter = _bumpFilter;
    _blockNode.shouldEnableEffects = YES;
    [_gameNode addChild:_blockNode];
    
    _currentLevel = levelNumber;
    [self setUpLevel:_currentLevel];
    
//    self.filter = [OldTimeyFilter new];
//    self.shouldEnableEffects = YES;
    
    [self inGameMessage:[NSString stringWithFormat:@"Level %i", _currentLevel]];
}

- (void)update:(NSTimeInterval)currentTime
{
    float distortionAmount = (float)sin(currentTime);
    [_bumpFilter setValue:@(distortionAmount) forKey:kCIInputScaleKey];
}

- (void)addCatBed
{
    _bedNode = [SKSpriteNode spriteNodeWithImageNamed:@"cat_bed"];
    _bedNode.position = CGPointMake(270, 15);
    
    [self addChild:_bedNode];
    
    CGSize contactSize = CGSizeMake(40, 30);
    _bedNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:contactSize];
    _bedNode.physicsBody.dynamic = NO;
    [_bedNode attachDebugRectWithSize:contactSize];
    
    _bedNode.physicsBody.categoryBitMask = CNPhysicsCategoryBed;
}

- (void)addCatAtPosition:(CGPoint)pos
{
    _catNode = [SKSpriteNode spriteNodeWithImageNamed:@"cat_sleepy"];
    _catNode.position = pos;
    
    [_gameNode addChild:_catNode];
    
    CGSize contactSize = CGSizeMake(_catNode.size.width-40, _catNode.size.height-10);
    
    _catNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:contactSize];
    
    _catNode.physicsBody.categoryBitMask = CNPhysicsCategoryCat;
    
    [_catNode attachDebugRectWithSize:contactSize];
    
    _catNode.physicsBody.collisionBitMask = CNPhysicsCategoryBlock | CNPhysicsCategoryEdge | CNPhysicsCategorySpring;
    
    _catNode.physicsBody.contactTestBitMask = CNPhysicsCategoryBed | CNPhysicsCategoryEdge;
}

- (void)setUpLevel:(int)levelNum
{
    NSString *fileName = [NSString stringWithFormat:@"level%i", levelNum];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    NSDictionary *level = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    [self addCatAtPosition:CGPointFromString(level[@"catPosition"])];
    
    [self addBlocksFromArray:level[@"blocks"]];
    
    [self addSpringsFromArray:level[@"springs"]];
    
    if (level[@"seesawPosition"]) {
            [self addSeesawAtPosition:CGPointFromString(level[@"seesawPosition"])];
    }

    if (level[@"hookPosition"]) {
        [self addHookAtPosition:CGPointFromString(level[@"hookPosition"])];
    }
    
    [[SKTAudio sharedInstance] playBackgroundMusic:@"bgMusic.mp3"];
}

- (void)addBlocksFromArray:(NSArray *)blocks
{
    for (NSDictionary *block in blocks){
        
        NSString *blockType = block[@"type"];
        
        if (!blockType) {
            
            if (block[@"tuple"]) {
                CGRect rect1 = CGRectFromString([block[@"tuple"] firstObject]);
                SKSpriteNode *block1 = [self addBlockWithRect:rect1];
                block1.physicsBody.friction = 0.8;
                block1.physicsBody.categoryBitMask = CNPhysicsCategoryBlock;
                block1.physicsBody.collisionBitMask = CNPhysicsCategoryBlock | CNPhysicsCategoryCat | CNPhysicsCategoryEdge;
                [_blockNode addChild:block1];
                
                CGRect rect2 = CGRectFromString([block[@"tuple"] lastObject]);
                SKSpriteNode *block2 = [self addBlockWithRect:rect2];
                block2.physicsBody.friction = 0.8;
                block2.physicsBody.categoryBitMask = CNPhysicsCategoryBlock;
                block2.physicsBody.collisionBitMask = CNPhysicsCategoryBlock | CNPhysicsCategoryCat | CNPhysicsCategoryEdge;
                [_blockNode addChild:block2];
                
                [self.physicsWorld addJoint:[SKPhysicsJointFixed jointWithBodyA:block1.physicsBody bodyB:block2.physicsBody anchor:CGPointZero]];
                
            } else {
                SKSpriteNode *blockSprite = [self addBlockWithRect:CGRectFromString(block[@"rect"])];
                
                blockSprite.physicsBody.categoryBitMask = CNPhysicsCategoryBlock;
                blockSprite.physicsBody.collisionBitMask = CNPhysicsCategoryBlock | CNPhysicsCategoryCat | CNPhysicsCategoryEdge;
                
                [_blockNode addChild:blockSprite];
            }
        } else {
            if ([blockType isEqualToString:@"PhotoFrameBlock"]) {
                [self createPhotoFrameWithPosition:CGPointFromString(block[@"point"])];
            } else if ([blockType isEqualToString:@"TVBlock"]) {
                [_blockNode addChild:[[OldTVNode alloc] initWithRect:CGRectFromString(block[@"rect"])]];
            } else if ([blockType isEqualToString:@"WonkyBlock"]) {
                [_blockNode addChild:[self createWonkyBlockFromRect:CGRectFromString(block[@"rect"])]];
            }
        }
    }
}

- (void)addSpringsFromArray:(NSArray *)springs
{
    for (NSDictionary *spring in springs ){
        SKSpriteNode *springSprite = [SKSpriteNode spriteNodeWithImageNamed:@"spring"];
        springSprite.position = CGPointFromString(spring[@"position"]);
        
        springSprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:springSprite.size];
        springSprite.physicsBody.categoryBitMask = CNPhysicsCategorySpring;
        springSprite.physicsBody.collisionBitMask = CNPhysicsCategoryEdge | CNPhysicsCategoryCat | CNPhysicsCategoryBlock;
        
        [springSprite attachDebugRectWithSize:springSprite.size];
        
        [_gameNode addChild:springSprite];
    }
}

- (SKSpriteNode *)addBlockWithRect:(CGRect)blockRect
{
    NSString *textureName = [NSString stringWithFormat:@"%.fx%.f.png", blockRect.size.width, blockRect.size.height];
    
    SKSpriteNode *blockSprite = [SKSpriteNode spriteNodeWithImageNamed:textureName];
    
    blockSprite.position = blockRect.origin;
    
    CGRect bodyRect = CGRectInset(blockRect, 2, 2);
    
    blockSprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bodyRect.size];
    
    [blockSprite attachDebugRectWithSize:bodyRect.size];
    
    return blockSprite;
}

- (void)addHookAtPosition:(CGPoint)hookPosition
{
    _hookBaseNode = nil;
    _hookNode = nil;
    _ropeNode = nil;
    
    _isHooked = NO;
    
    _hookBaseNode = [SKSpriteNode spriteNodeWithImageNamed:@"hook_base"];
    _hookBaseNode.position = CGPointMake(hookPosition.x, hookPosition.y - _hookBaseNode.size.height/2);
    _hookBaseNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_hookBaseNode.size];
    
    [_gameNode addChild:_hookBaseNode];
    
    SKPhysicsJointFixed *ceilingFix = [SKPhysicsJointFixed jointWithBodyA:_hookBaseNode.physicsBody bodyB:self.physicsBody anchor:CGPointZero];
    [self.physicsWorld addJoint:ceilingFix];
    
    
    _ropeNode = [SKSpriteNode spriteNodeWithImageNamed:@"rope"];
    _ropeNode.anchorPoint = CGPointMake(0, 0.5);
    _ropeNode.position = _hookBaseNode.position;
    [_gameNode addChild:_ropeNode];
    
    _hookNode = [SKSpriteNode spriteNodeWithImageNamed:@"hook"];
    _hookNode.position = CGPointMake(hookPosition.x, hookPosition.y-63);
    
    _hookNode.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:_hookNode.size.width/2];
    _hookNode.physicsBody.categoryBitMask = CNPhysicsCategoryHook;
    _hookNode.physicsBody.contactTestBitMask = CNPhysicsCategoryCat;
    _hookNode.physicsBody.collisionBitMask = kNilOptions;
    
    [_gameNode addChild:_hookNode];
    
    SKPhysicsJointSpring *ropeJoint = [SKPhysicsJointSpring jointWithBodyA:_hookBaseNode.physicsBody bodyB:_hookNode.physicsBody anchorA:_hookBaseNode.position anchorB:CGPointMake(_hookNode.position.x, _hookNode.position.y+_hookNode.size.height/2)];
    
    [self.physicsWorld addJoint:ropeJoint];
}

- (void)addSeesawAtPosition:(CGPoint)seesawPosition
{
    SKSpriteNode *seesawBaseNode = [SKSpriteNode spriteNodeWithImageNamed:@"45x45"];
    seesawBaseNode.position = seesawPosition;
    seesawBaseNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: seesawBaseNode.size];
    seesawBaseNode.physicsBody.categoryBitMask = kNilOptions;
    seesawBaseNode.physicsBody.collisionBitMask = kNilOptions;
    [_gameNode addChild:seesawBaseNode];
    
    SKPhysicsJointFixed *baseFix = [SKPhysicsJointFixed jointWithBodyA:seesawBaseNode.physicsBody bodyB:self.physicsBody anchor:CGPointZero];
    [self.physicsWorld addJoint:baseFix];
    
    SKSpriteNode *seesawNode = [SKSpriteNode spriteNodeWithImageNamed:@"430x30"];
    seesawNode.position = seesawPosition;
    seesawNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: seesawNode.size];
    seesawNode.physicsBody.collisionBitMask = CNPhysicsCategoryCat | CNPhysicsCategoryBlock;
    [seesawNode attachDebugRectWithSize: seesawNode.size];
    [_gameNode addChild:seesawNode];
    
    SKPhysicsJointPin *seesawPin = [SKPhysicsJointPin jointWithBodyA:seesawNode.physicsBody bodyB:seesawBaseNode.physicsBody anchor:seesawPosition];
    [self.physicsWorld addJoint:seesawPin];

    
}

- (SKShapeNode *)createWonkyBlockFromRect:(CGRect)inputRect
{

    CGPoint origin =CGPointMake(inputRect.origin.x - inputRect.size.width / 2.0,
                inputRect.origin.y - inputRect.size.height/2.0);
    CGPoint pointlb = origin;
    CGPoint pointlt =CGPointMake(origin.x, inputRect.origin.y + inputRect.size.height);
    CGPoint pointrb =CGPointMake(origin.x + inputRect.size.width, origin.y);
    CGPoint pointrt =CGPointMake(origin.x + inputRect.size.width, origin.y + inputRect.size.height);
    
    pointlb = adjustedPoint(pointlb, inputRect.size);
    pointlt = adjustedPoint(pointlt, inputRect.size);
    pointrb = adjustedPoint(pointrb, inputRect.size);
    pointrt = adjustedPoint(pointrt, inputRect.size);
    

    UIBezierPath *shapeNodePath = [UIBezierPath bezierPath];
    [shapeNodePath moveToPoint:pointlb];
    [shapeNodePath addLineToPoint:pointlt];
    [shapeNodePath addLineToPoint:pointrt];
    [shapeNodePath addLineToPoint:pointrb];
    

    [shapeNodePath closePath];
    

    SKShapeNode *wonkyBlock = [SKShapeNode node];
    wonkyBlock.path = shapeNodePath.CGPath;
    
 
    UIBezierPath *physicsBodyPath = [UIBezierPath bezierPath];
    [physicsBodyPath moveToPoint:CGPointSubtract(pointlb, CGPointMake(-2, -2))];
    [physicsBodyPath addLineToPoint:CGPointSubtract(pointlt, CGPointMake(-2, 2))];
    [physicsBodyPath addLineToPoint:CGPointSubtract(pointrt, CGPointMake(2, 2))];
    [physicsBodyPath addLineToPoint:CGPointSubtract(pointrb, CGPointMake(2, -2))];
    [physicsBodyPath closePath];
    

    wonkyBlock.physicsBody =[SKPhysicsBody bodyWithPolygonFromPath:physicsBodyPath.CGPath];
    wonkyBlock.physicsBody.categoryBitMask =CNPhysicsCategoryBlock;
    wonkyBlock.physicsBody.collisionBitMask =CNPhysicsCategoryBlock | CNPhysicsCategoryCat |
    CNPhysicsCategoryEdge;
    

    wonkyBlock.lineWidth = 1.0;
    wonkyBlock.fillColor =[SKColor colorWithRed:0.75 green:0.75 blue:1.0 alpha:1.0];
    wonkyBlock.strokeColor =[SKColor colorWithRed:0.15 green:0.15 blue:0.0 alpha:1.0];
    wonkyBlock.glowWidth = 1.0;
    return wonkyBlock;
}

- (void)releaseHook
{
    _catNode.zRotation = 0;
    
    [self.physicsWorld removeJoint:[_hookNode.physicsBody.joints lastObject]];
    _isHooked = NO;
}

- (void)inGameMessage:(NSString *)text
{
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"AvenirNext-Regular"];
    label.text = text;
    label.fontSize = 64.0;
    label.color = [SKColor whiteColor];
    
    label.position = CGPointMake(self.frame.size.width/2, self.frame.size.height - 10);
    label.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:10];
    label.physicsBody.collisionBitMask = CNPhysicsCategoryEdge;
    label.physicsBody.categoryBitMask = CNPhysicsCategoryLabel;
    label.physicsBody.contactTestBitMask = CNPhysicsCategoryEdge;
    label.physicsBody.restitution = 0.7;
    
    [_gameNode addChild:label];
    
    
//    [label runAction:[SKAction sequence:@[[SKAction waitForDuration:3.0], [SKAction removeFromParent]]]];
}

- (void)createPhotoFrameWithPosition:(CGPoint)position
{
    SKSpriteNode *photoFrame = [SKSpriteNode spriteNodeWithImageNamed:@"picture-frame"];
    photoFrame.name = @"PhotoFrameNode";
    photoFrame.position = position;
    
    SKSpriteNode *pictureNode = [SKSpriteNode spriteNodeWithImageNamed:@"picture"];
    pictureNode.name = @"PictureNode";
    
    SKSpriteNode *maskNode = [SKSpriteNode spriteNodeWithImageNamed:@"picture-frame-mask"];
    maskNode.name = @"Mask";
    
    SKCropNode *cropNode = [SKCropNode node];
    [cropNode addChild:pictureNode];
    [cropNode setMaskNode:maskNode];
    [photoFrame addChild:cropNode];

    [_gameNode addChild:photoFrame];
    
    photoFrame.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:photoFrame.size.width/2.0 - photoFrame.size.width * 0.025];
    photoFrame.physicsBody.categoryBitMask = CNPhysicsCategoryBlock;
    photoFrame.physicsBody.collisionBitMask = CNPhysicsCategoryBlock | CNPhysicsCategoryCat | CNPhysicsCategoryEdge;
}

- (void)lose
{
    
    if (_currentLevel > 1) {
        _currentLevel--;
    }
    
    _catNode.physicsBody.contactTestBitMask = 0;
    [_catNode setTexture:[SKTexture textureWithImageNamed:@"cat_awake"]];
    
    [[SKTAudio sharedInstance] pauseBackgroundMusic];
    [self runAction:[SKAction playSoundFileNamed:@"lose.mp3" waitForCompletion:NO]];
    
    [self inGameMessage:@"Try again ..."];
    
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:5.0], [SKAction performSelector:@selector(newGame) onTarget:self]]]];
}

- (void)win
{
    if (_currentLevel < 7) {
        _currentLevel++;
    }
    
    _catNode.physicsBody = nil;
    
    CGFloat curlY = _bedNode.position.y+_catNode.size.height/2;
    CGPoint curlPoint = CGPointMake(_bedNode.position.x, curlY);
    
    [_catNode runAction:[SKAction group:@[[SKAction moveTo:curlPoint duration:0.66], [SKAction rotateToAngle:0 duration:0.5]]]];
    
    [self inGameMessage:@"Good job!"];
    
    [self runAction:[SKAction sequence:@[[SKAction waitForDuration:5.0], [SKAction performSelector:@selector(newGame) onTarget:self]]]];
    
    [_catNode runAction:[SKAction animateWithTextures:@[[SKTexture textureWithImageNamed:@"cat_curlup1"],
                                                        [SKTexture textureWithImageNamed:@"cat_curlup2"],
                                                        [SKTexture textureWithImageNamed:@"cat_curlup3"]]
                                         timePerFrame:0.25]];
    [[SKTAudio sharedInstance] pauseBackgroundMusic];
    [self runAction:[SKAction playSoundFileNamed:@"win.mp3" waitForCompletion:NO]];
    
}


- (void)newGame
{
    SKScene *nextLevel = [[MyScene alloc] initWithSize:self.size andLevelNumber:_currentLevel];
    SKTransition *levelTransition = [SKTransition transitionWithCIFilter:[CustomTransitionFilter new] duration:0.5];
    [self.view presentScene:nextLevel transition:levelTransition];
}

#pragma mark - Touch Events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    [self.physicsWorld enumerateBodiesAtPoint:location usingBlock:^(SKPhysicsBody *body, BOOL *stop) {
        if ([body.node.name isEqualToString:@"PhotoFrameNode"]) {
            [self.delegate requestImagePicker];
            *stop = YES;
            return;
        }
        
        if (body.categoryBitMask == CNPhysicsCategoryBlock) {
            
            for (SKPhysicsJoint *joint in body.joints){
                [self.physicsWorld removeJoint:joint];
                [joint.bodyA.node removeFromParent];
                [joint.bodyB.node removeFromParent];
            }
            
            [body.node removeFromParent];
            *stop = YES;
            
            [self runAction:[SKAction playSoundFileNamed:@"pop.mp3" waitForCompletion:NO]];
        }
        
        if (body.categoryBitMask == CNPhysicsCategorySpring) {
            SKSpriteNode *spring = (SKSpriteNode *)body.node;
            
            [body applyImpulse:CGVectorMake(0, 12) atPoint:CGPointMake(spring.size.width/2, spring.size.height)];
            [body.node runAction:[SKAction sequence:@[[SKAction waitForDuration:1], [SKAction removeFromParent]]]];
            
            *stop = YES;
        }
        
        if (body.categoryBitMask == CNPhysicsCategoryCat && _isHooked) {
            [self releaseHook];
        }
    }];
}

#pragma mark - SKPhysicsContactDelegate

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    uint32_t collision = (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask);
    if (collision == (CNPhysicsCategoryCat | CNPhysicsCategoryBed)) {
        [self win];
    }
    
    if (collision == (CNPhysicsCategoryCat | CNPhysicsCategoryEdge)) {
        [self lose];
    }
    
    if (collision == (CNPhysicsCategoryLabel | CNPhysicsCategoryEdge)) {
        SKLabelNode *label = contact.bodyA.categoryBitMask == CNPhysicsCategoryLabel ? (SKLabelNode *)contact.bodyA.node : (SKLabelNode *)contact.bodyB.node;
        if (label.userData == nil) {
            label.userData = [NSMutableDictionary dictionaryWithDictionary:@{@"bounceCount": @1}];
        } else {
            NSNumber *count = label.userData[@"bounceCount"];
            NSInteger bounces = [count integerValue];
            
            if (bounces >= 4) {
                [label removeFromParent];
            } else {
                label.userData = [NSMutableDictionary dictionaryWithDictionary:@{@"bounceCount": @(++bounces)}];
            }
            
        }
    }
    
    if (collision == (CNPhysicsCategoryHook | CNPhysicsCategoryCat)) {
        _catNode.physicsBody.velocity = CGVectorMake(0, 0);
        _catNode.physicsBody.angularVelocity = 0;
        
        SKPhysicsJointFixed *hookJoint = [SKPhysicsJointFixed jointWithBodyA:_hookNode.physicsBody bodyB:_catNode.physicsBody anchor:CGPointMake(_hookNode.position.x, _hookNode.position.y + _hookNode.size.height/2)];
        
        [self.physicsWorld addJoint:hookJoint];
        
        _isHooked = YES;
    }
}

- (void)setPhotoTexture:(SKTexture *)texture
{
    SKSpriteNode *picture = (SKSpriteNode *)[self childNodeWithName:@"//PictureNode"];
    [picture setTexture:texture];
}

@end
