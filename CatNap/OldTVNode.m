//
//  OldTVNode.m
//  CatNap
//
//  Created by Brandon Levasseur on 12/31/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "OldTVNode.h"
#import <AVFoundation/AVFoundation.h>
#import "Physics.h"

AVPlayer *_player;
SKVideoNode *_videoNode;

@implementation OldTVNode

-(instancetype)initWithRect:(CGRect)frame
{
    if (self = [super initWithImageNamed:@"tv"]) {
        self.name = @"TVNode";
        
        SKSpriteNode *tvMaskNode = [SKSpriteNode spriteNodeWithImageNamed:@"tv-mask"];
        tvMaskNode.size = frame.size;
        SKCropNode *cropNode = [SKCropNode node];
        cropNode.maskNode = tvMaskNode;
        
        NSURL *fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"loop" ofType:@"mov"]];
        _player = [AVPlayer playerWithURL:fileURL];
        _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserverForName:AVPlayerItemDidPlayToEndTimeNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
            [_player seekToTime:kCMTimeZero];
        }];
        
        _videoNode = [[SKVideoNode alloc] initWithAVPlayer:_player];
        _videoNode.size = CGRectInset(frame, frame.size.width * .15, frame.size.height * .27).size;
        _videoNode.position = CGPointMake(-frame.size.width * .1, -frame.size.height * .06);
        
        [cropNode addChild:_videoNode];
        [self addChild:cropNode];
        
        self.position = frame.origin;
        self.size = frame.size;
        
        _player.volume = 0.0;
        
        CGRect bodyRect = CGRectInset(frame, 2, 2);
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bodyRect.size];
        self.physicsBody.categoryBitMask = CNPhysicsCategoryBlock;
        self.physicsBody.collisionBitMask = CNPhysicsCategoryBlock | CNPhysicsCategoryCat | CNPhysicsCategoryEdge;
        
        [_videoNode play];
        
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

@end
