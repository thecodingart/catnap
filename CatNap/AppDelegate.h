//
//  AppDelegate.h
//  CatNap
//
//  Created by Brandon Levasseur on 12/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
