//
//  SKSpriteNode+DebugDraw.h
//  CatNap
//
//  Created by Brandon Levasseur on 12/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKSpriteNode (DebugDraw)

-(void)attachDebugRectWithSize:(CGSize)s;
-(void)attachDebugFrameFromPath:(CGPathRef)bodyPath;

@end
