//
//  OldTimeyFilter.m
//  CatNap
//
//  Created by Brandon Levasseur on 1/31/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import "OldTimeyFilter.h"
#import "perlin.h"

@implementation OldTimeyFilter


- (CIImage *)outputImage
{
    CFAbsoluteTime time = CFAbsoluteTimeGetCurrent();
    float v1[] = {sin(time / 15.0) * 100, 1.5};
    float v2[] = {sin(time / 2.0) * 25, 1.5};
    double randVal1 = noise2(v1);
    double randVal2 = noise2(v2);
    
    CIFilter *colorControls = [CIFilter filterWithName:@"CIColorControls"];
    [colorControls setValue:@0.0 forKey:kCIInputSaturationKey];
    [colorControls setValue:@(randVal2 * .2) forKey:kCIInputBrightnessKey];
    
    CIFilter *vignette = [CIFilter filterWithName:@"CIVignette"];
    [vignette setValue:@(0.2 + randVal2) forKey:kCIInputRadiusKey];
    [vignette setValue:@(randVal2 * .2 + 0.8) forKey:kCIInputIntensityKey];
    
    CIFilter *transformFilter = [CIFilter filterWithName:@"CIAffineTransform"];
    CGAffineTransform t = CGAffineTransformMakeTranslation(0.0, randVal1 * 45.0);
    NSValue *transform = [NSValue valueWithCGAffineTransform:t];
    [transformFilter setValue:transform forKey:kCIInputTransformKey];
    
    [colorControls setValue:self.inputImage forKey:kCIInputImageKey];
    [vignette setValue:colorControls.outputImage forKey:kCIInputImageKey];
    [transformFilter setValue:vignette.outputImage forKey:kCIInputImageKey];
    
    return transformFilter.outputImage;
    
}

@end
